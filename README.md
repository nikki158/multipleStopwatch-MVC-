# MultipleStopwatch with MVC using Bootsrap framework
![Desktop](https://i.imgur.com/CVvTeFS.png)
-------------------------------------------------------------------------------------
+ A stopwatch which has 3 counters to clock multiple elapse time of events parallely.
+ Has ways to start, stop and reset all counters. 
+ You can record split timing for each counter which displays respective **Total time** and **Lap time**.
+ You can download **stop watch history** template for each counters.
